public class Kata {

    public static int mostFrequentItemCount(int[] collection) {
            int mostFrequent = 0;

            for( int i = 0; i < collection.length; i++) {
                int counter =  1;
                for ( int i1 = i + 1 ; i1 < collection.length; i1++) {
                    if(collection[i1] == collection[i])
                        counter++;
                }
                mostFrequent = (counter > mostFrequent) ? counter : mostFrequent;
            }
            return mostFrequent;
        }
}
