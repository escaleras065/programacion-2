package LombolBuilder;

public class LombokBuilder {

    //We want to:
    //
    //Encapsulate properly the class by providing read accessors (setters are not required for this Kata).

    public class People{
        public int age;
        public String name;
        public String lastName;
        String GREET="hello";

        public String greet(){
            return GREET+" my name is "+name;
        }
    }

    public class peopleCorrect{

        private int age;
        private String name;
        private String lastName;
        private String greet = "hello";

        public String greet(){
            return greet + " my name is " + name;
        }
    }
}
