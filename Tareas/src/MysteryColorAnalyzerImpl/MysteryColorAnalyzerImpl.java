package MysteryColorAnalyzerImpl;

import java.util.List;

//Create the implementation for the interface below. The implementation needs to be called "MysteryColorAnalyzerImpl
public class MysteryColorAnalyzerImpl {
    /**
     * This method will determine how many distinct colors are in the given list
     *
     * <p>
     * Colors are defined via the {@link Color} enum.
     * </p>
     *
     * @param mysteryColors list of colors from which to determine the number of distinct colors
     * @return number of distinct colors
     */
    public int numberOfDistinctColors(List<Color> mysteryColors){
        int numberOfColors = 0;
        if(numberOfColors < mysteryColors.size()){
            numberOfColors++;
        }
        return numberOfColors;
    }

    /**
     * This method will count how often a specific color is in the given list
     *
     * <p>
     * Colors are defined via the {@link Color} enum.
     * </p>
     *
     * @param mysteryColors list of colors from which to determine the count of a specific color
     * @param color         color to count
     * @return number of occurrence of the color in the list
     */
    public int colorOccurrence(List<Color> mysteryColors, Color color) {
        int colorRepit = 0;
        for (int i=0;color==mysteryColors.get(i);i++){
            colorRepit++;
        }
        return colorRepit;
    }

    public enum Color {
        RED, GREEN, BLUE
    }
}
