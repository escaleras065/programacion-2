import MostFrequentArray.Kata;
import org.junit.Assert;
import org.junit.Test;

public class FrequentExampleTests {

    public FrequentExampleTests() {
    }

    @Test
    public void tests() {
        Assert.assertEquals(2L, (long)Kata.mostFrequentItemCount(new int[]{3, -1, -1}));
        Assert.assertEquals(5L, (long) Kata.mostFrequentItemCount(new int[]{3, -1, -1, -1, 2, 3, -1, 3, -1, 2, 4, 9, 3}));
    }
}
