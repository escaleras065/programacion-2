public class Canvas {

    private int width;
    private int height;

    public Canvas(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public void draw(int width, int height) {
        int counterHeight = 1;
        while (counterHeight <= height){
            System.out.print("|");
            int counterWidth = 1;
            while (counterWidth <= width){
                System.out.print("-");
                counterWidth++;
            }
            counterHeight++;
            System.out.println("|");
        }

    }

    public static void main(String[] args) {
        Canvas canvas = new Canvas(2,2);
        canvas.draw(2,2);
    }
}
